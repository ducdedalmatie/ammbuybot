require('dotenv').config();
const Web3 = require('web3');
const ethers = require('ethers');

// var add = 'wss://bsc-ws-node.nariox.org:443';
const add = process.env.BSC_RPC_URL_WSS;

const PANCAKE_ROUTER_ADDRESS = '0x10ed43c718714eb63d5aa57b78b54704e256024e';
// const PANCAKE_FACTORY_ADDRESS = '0xbcfccbde45ce874adcb698cc183debcf17952812';

const { RECEIVE_WALLET_ADDRESS } = process.env;
const { WALLET_PRIVATE_KEY } = process.env;
// const WETH1 = '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c'; //WBNB - token to spend
// const WETH1 = '0xe9e7cea3dedca5984780bafc599bd69add087d56';//BUSD - token to spend
// 0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d // USDC
// 0x55d398326f99059fF775485246999027B3197955 // USDT
// 0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3 // DAI
// 0x8076c74c5e3f5852037f31ff0093eeb8c8add8d3 //safemoon

const SELL_TOKEN_ADDRESS = process.env.SELL_TOKEN_ADDRESS.toLowerCase(); // Token to sell
const SELL_TOKEN_ID = SELL_TOKEN_ADDRESS.substring(2, 42);
const BUY_TOKEN_ADDRESS = process.env.BUY_TOKEN_ADDRESS.toLowerCase(); // token to buy.
// Same as TokenID but don't remove 0x for sniping. Use any other for testing.
const BUY_TOKEN_ID = BUY_TOKEN_ADDRESS.substring(2, 42);

// const defaultRPCUrl = 'https://bsc-dataseed1.defibit.io:443';

const defaultProvider = new Web3(
  new Web3.providers.HttpProvider(process.env.BSC_RPC_URL),
);

const signer = defaultProvider.eth.accounts.privateKeyToAccount(WALLET_PRIVATE_KEY);
const SELL_WALLET_ADDRESS = signer.address;

const amountIn = process.env.SELL_TOKEN_AMOUNT; // token amount to buy
// var amountInMax = process.env.SELL_TOKEN_AMOUNT_MAX; // max token amount to sell.

const amountOutMin = ethers.utils.parseUnits('0', 'ether');

// const gasPrice = process.env.GAS;

let flag = 0;

const UNISWAP_ROUTER_ABI = [
  {
    inputs: [
      { internalType: 'address', name: '_factory', type: 'address' },
      { internalType: 'address', name: '_WETH', type: 'address' },
    ],
    stateMutability: 'nonpayable',
    type: 'constructor',
  },
  {
    inputs: [],
    name: 'WETH',
    outputs: [{ internalType: 'address', name: '', type: 'address' }],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'address', name: 'tokenA', type: 'address' },
      { internalType: 'address', name: 'tokenB', type: 'address' },
      { internalType: 'uint256', name: 'amountADesired', type: 'uint256' },
      { internalType: 'uint256', name: 'amountBDesired', type: 'uint256' },
      { internalType: 'uint256', name: 'amountAMin', type: 'uint256' },
      { internalType: 'uint256', name: 'amountBMin', type: 'uint256' },
      { internalType: 'address', name: 'to', type: 'address' },
      { internalType: 'uint256', name: 'deadline', type: 'uint256' },
    ],
    name: 'addLiquidity',
    outputs: [
      { internalType: 'uint256', name: 'amountA', type: 'uint256' },
      { internalType: 'uint256', name: 'amountB', type: 'uint256' },
      { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
    ],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'address', name: 'token', type: 'address' },
      { internalType: 'uint256', name: 'amountTokenDesired', type: 'uint256' },
      { internalType: 'uint256', name: 'amountTokenMin', type: 'uint256' },
      { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
      { internalType: 'address', name: 'to', type: 'address' },
      { internalType: 'uint256', name: 'deadline', type: 'uint256' },
    ],
    name: 'addLiquidityETH',
    outputs: [
      { internalType: 'uint256', name: 'amountToken', type: 'uint256' },
      { internalType: 'uint256', name: 'amountETH', type: 'uint256' },
      { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
    ],
    stateMutability: 'payable',
    type: 'function',
  },
  {
    inputs: [],
    name: 'factory',
    outputs: [{ internalType: 'address', name: '', type: 'address' }],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
      { internalType: 'uint256', name: 'reserveIn', type: 'uint256' },
      { internalType: 'uint256', name: 'reserveOut', type: 'uint256' },
    ],
    name: 'getAmountIn',
    outputs: [{ internalType: 'uint256', name: 'amountIn', type: 'uint256' }],
    stateMutability: 'pure',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
      { internalType: 'uint256', name: 'reserveIn', type: 'uint256' },
      { internalType: 'uint256', name: 'reserveOut', type: 'uint256' },
    ],
    name: 'getAmountOut',
    outputs: [{ internalType: 'uint256', name: 'amountOut', type: 'uint256' }],
    stateMutability: 'pure',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
      { internalType: 'address[]', name: 'path', type: 'address[]' },
    ],
    name: 'getAmountsIn',
    outputs: [{ internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' }],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
      { internalType: 'address[]', name: 'path', type: 'address[]' },
    ],
    name: 'getAmountsOut',
    outputs: [{ internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' }],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: 'amountA', type: 'uint256' },
      { internalType: 'uint256', name: 'reserveA', type: 'uint256' },
      { internalType: 'uint256', name: 'reserveB', type: 'uint256' },
    ],
    name: 'quote',
    outputs: [{ internalType: 'uint256', name: 'amountB', type: 'uint256' }],
    stateMutability: 'pure',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'address', name: 'tokenA', type: 'address' },
      { internalType: 'address', name: 'tokenB', type: 'address' },
      { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
      { internalType: 'uint256', name: 'amountAMin', type: 'uint256' },
      { internalType: 'uint256', name: 'amountBMin', type: 'uint256' },
      { internalType: 'address', name: 'to', type: 'address' },
      { internalType: 'uint256', name: 'deadline', type: 'uint256' },
    ],
    name: 'removeLiquidity',
    outputs: [
      { internalType: 'uint256', name: 'amountA', type: 'uint256' },
      { internalType: 'uint256', name: 'amountB', type: 'uint256' },
    ],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'address', name: 'token', type: 'address' },
      { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
      { internalType: 'uint256', name: 'amountTokenMin', type: 'uint256' },
      { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
      { internalType: 'address', name: 'to', type: 'address' },
      { internalType: 'uint256', name: 'deadline', type: 'uint256' },
    ],
    name: 'removeLiquidityETH',
    outputs: [
      { internalType: 'uint256', name: 'amountToken', type: 'uint256' },
      { internalType: 'uint256', name: 'amountETH', type: 'uint256' },
    ],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'address', name: 'token', type: 'address' },
      { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
      { internalType: 'uint256', name: 'amountTokenMin', type: 'uint256' },
      { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
      { internalType: 'address', name: 'to', type: 'address' },
      { internalType: 'uint256', name: 'deadline', type: 'uint256' },
    ],
    name: 'removeLiquidityETHSupportingFeeOnTransferTokens',
    outputs: [{ internalType: 'uint256', name: 'amountETH', type: 'uint256' }],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'address', name: 'token', type: 'address' },
      { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
      { internalType: 'uint256', name: 'amountTokenMin', type: 'uint256' },
      { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
      { internalType: 'address', name: 'to', type: 'address' },
      { internalType: 'uint256', name: 'deadline', type: 'uint256' },
      { internalType: 'bool', name: 'approveMax', type: 'bool' },
      { internalType: 'uint8', name: 'v', type: 'uint8' },
      { internalType: 'bytes32', name: 'r', type: 'bytes32' },
      { internalType: 'bytes32', name: 's', type: 'bytes32' },
    ],
    name: 'removeLiquidityETHWithPermit',
    outputs: [
      { internalType: 'uint256', name: 'amountToken', type: 'uint256' },
      { internalType: 'uint256', name: 'amountETH', type: 'uint256' },
    ],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'address', name: 'token', type: 'address' },
      { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
      { internalType: 'uint256', name: 'amountTokenMin', type: 'uint256' },
      { internalType: 'uint256', name: 'amountETHMin', type: 'uint256' },
      { internalType: 'address', name: 'to', type: 'address' },
      { internalType: 'uint256', name: 'deadline', type: 'uint256' },
      { internalType: 'bool', name: 'approveMax', type: 'bool' },
      { internalType: 'uint8', name: 'v', type: 'uint8' },
      { internalType: 'bytes32', name: 'r', type: 'bytes32' },
      { internalType: 'bytes32', name: 's', type: 'bytes32' },
    ],
    name: 'removeLiquidityETHWithPermitSupportingFeeOnTransferTokens',
    outputs: [{ internalType: 'uint256', name: 'amountETH', type: 'uint256' }],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'address', name: 'tokenA', type: 'address' },
      { internalType: 'address', name: 'tokenB', type: 'address' },
      { internalType: 'uint256', name: 'liquidity', type: 'uint256' },
      { internalType: 'uint256', name: 'amountAMin', type: 'uint256' },
      { internalType: 'uint256', name: 'amountBMin', type: 'uint256' },
      { internalType: 'address', name: 'to', type: 'address' },
      { internalType: 'uint256', name: 'deadline', type: 'uint256' },
      { internalType: 'bool', name: 'approveMax', type: 'bool' },
      { internalType: 'uint8', name: 'v', type: 'uint8' },
      { internalType: 'bytes32', name: 'r', type: 'bytes32' },
      { internalType: 'bytes32', name: 's', type: 'bytes32' },
    ],
    name: 'removeLiquidityWithPermit',
    outputs: [
      { internalType: 'uint256', name: 'amountA', type: 'uint256' },
      { internalType: 'uint256', name: 'amountB', type: 'uint256' },
    ],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
      { internalType: 'address[]', name: 'path', type: 'address[]' },
      { internalType: 'address', name: 'to', type: 'address' },
      { internalType: 'uint256', name: 'deadline', type: 'uint256' },
    ],
    name: 'swapETHForExactTokens',
    outputs: [{ internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' }],
    stateMutability: 'payable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
      { internalType: 'address[]', name: 'path', type: 'address[]' },
      { internalType: 'address', name: 'to', type: 'address' },
      { internalType: 'uint256', name: 'deadline', type: 'uint256' },
    ],
    name: 'swapExactETHForTokens',
    outputs: [{ internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' }],
    stateMutability: 'payable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
      { internalType: 'address[]', name: 'path', type: 'address[]' },
      { internalType: 'address', name: 'to', type: 'address' },
      { internalType: 'uint256', name: 'deadline', type: 'uint256' },
    ],
    name: 'swapExactETHForTokensSupportingFeeOnTransferTokens',
    outputs: [],
    stateMutability: 'payable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
      { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
      { internalType: 'address[]', name: 'path', type: 'address[]' },
      { internalType: 'address', name: 'to', type: 'address' },
      { internalType: 'uint256', name: 'deadline', type: 'uint256' },
    ],
    name: 'swapExactTokensForETH',
    outputs: [{ internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' }],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
      { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
      { internalType: 'address[]', name: 'path', type: 'address[]' },
      { internalType: 'address', name: 'to', type: 'address' },
      { internalType: 'uint256', name: 'deadline', type: 'uint256' },
    ],
    name: 'swapExactTokensForETHSupportingFeeOnTransferTokens',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
      { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
      { internalType: 'address[]', name: 'path', type: 'address[]' },
      { internalType: 'address', name: 'to', type: 'address' },
      { internalType: 'uint256', name: 'deadline', type: 'uint256' },
    ],
    name: 'swapExactTokensForTokens',
    outputs: [{ internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' }],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: 'amountIn', type: 'uint256' },
      { internalType: 'uint256', name: 'amountOutMin', type: 'uint256' },
      { internalType: 'address[]', name: 'path', type: 'address[]' },
      { internalType: 'address', name: 'to', type: 'address' },
      { internalType: 'uint256', name: 'deadline', type: 'uint256' },
    ],
    name: 'swapExactTokensForTokensSupportingFeeOnTransferTokens',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
      { internalType: 'uint256', name: 'amountInMax', type: 'uint256' },
      { internalType: 'address[]', name: 'path', type: 'address[]' },
      { internalType: 'address', name: 'to', type: 'address' },
      { internalType: 'uint256', name: 'deadline', type: 'uint256' },
    ],
    name: 'swapTokensForExactETH',
    outputs: [{ internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' }],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: 'amountOut', type: 'uint256' },
      { internalType: 'uint256', name: 'amountInMax', type: 'uint256' },
      { internalType: 'address[]', name: 'path', type: 'address[]' },
      { internalType: 'address', name: 'to', type: 'address' },
      { internalType: 'uint256', name: 'deadline', type: 'uint256' },
    ],
    name: 'swapTokensForExactTokens',
    outputs: [{ internalType: 'uint256[]', name: 'amounts', type: 'uint256[]' }],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  { stateMutability: 'payable', type: 'receive' },
];
// let signer = new ethers.Wallet(WALLET_PRIVATE_KEY, defaultProvider);

// const SELL_WALLET_ADDRESS = signer.address;
// signer.getBalance().then(function(balance) {
//      amountInMax = balance.toString();
// });

const router = new defaultProvider.eth.Contract(
  UNISWAP_ROUTER_ABI,
  PANCAKE_ROUTER_ADDRESS,
  // [
  //   'function swapExactTokensForTokens(uint amountIn, uint amountOutMin,
  //       address[] calldata path, address to, uint deadline) external
  //         returns (uint[] memory amounts)',
  //   'function swapExactETHForTokens(uint amountOutMin,
  //    address[] calldata path, address to, uint deadline) external payable
  //         returns (uint[] memory amounts)',
  // ],
  // signer,
);

const web3 = new Web3(add);

const minABI = [
  // balanceOf
  {
    constant: true,
    inputs: [{ name: '_owner', type: 'address' }],
    name: 'balanceOf',
    outputs: [{ name: 'balance', type: 'uint256' }],
    type: 'function',
  },
  // decimals
  {
    constant: true,
    inputs: [],
    name: 'decimals',
    outputs: [{ name: '', type: 'uint8' }],
    type: 'function',
  },
];

const sellTokenContract = new web3.eth.Contract(minABI, SELL_TOKEN_ADDRESS);
let amountInString = '';

sellTokenContract.methods
  .balanceOf(SELL_WALLET_ADDRESS)
  .call()
  .then((balance) => {
    const amountInMax = balance;

    sellTokenContract.methods
      .decimals()
      .call()
      .then((decimals) => {
        if (amountInMax < amountIn * 10 ** decimals) {
          console.log(
            'Sell token is less than needed amount. Please check again and rerun...',
          );
          return;
        }
        amountInString = (amountIn * 10 ** decimals).toString();
      });
  });

// let buyTokenContract = new web3.eth.Contract(minABI, BUY_TOKEN_ADDRESS);
// var buyTokenDecimals = 0;

// buyTokenContract.methods.decimals().call().then((decimals) => {
//       buyTokenDecimals = decimals;
// });

// let abi = ["function approve(address _spender, uint256 _value) public returns (bool success)"];
// let contract = new ethers.Contract(BUY_TOKEN_ADDRESS, abi, signer);
// contract.approve(RECEIVE_WALLET_ADDRESS, amountOutString);
// console.log("Waiting for addLiquidity done...");

// function parseTx(input) {
//   if (input === '0x') {
//     return ['0x', []];
//   }
//   if ((input.length - 8 - 2) % 64 !== 0) {
//     throw new Error('Data size misaligned with parse request.');
//   }
// }

const addLiquidityMethodId = '0xe8e33700';
const addLiquidityETHMethodId = '0xf305d719';

const subscription = web3.eth.subscribe('pendingTransactions', (err) => {
  if (err) console.error(err);
});

subscription.on('data', (txHash) => {
  setTimeout(async () => {
    try {
      // console.log(txHash);
      // return;

      if (flag === 1) return;

      const tx = await web3.eth.getTransaction(txHash);

      if (tx == null) {
        return;
      }

      let deadline = 0;
      if (tx.input) {
        if (tx.to && tx.to.toLowerCase() === PANCAKE_ROUTER_ADDRESS) {
          if (
            tx.input.includes(addLiquidityMethodId)
            && tx.input.includes(BUY_TOKEN_ID)
            && tx.input.includes(SELL_TOKEN_ID)
          ) {
            // let data = parseTx(tx.input);
            // let method = data[0];
            //                  let params = data[1];

            flag = 1;
            console.log('Detect addLiquidity!!!');
            console.log('type 1');
            // let abi = ["function approve(address _spender, uint256 _value)
            //               public returns (bool success)"];
            // let contract = new ethers.Contract(token_to_buy, abi, signer);
            // contract.approve(RECEIVE_ADDRESS, (amountOut * (10**18)).toString());
            // console.log("Waiting for addLiquidity done...");

            // var receipt = null;
            //   do {
            //     receipt = await web3.eth.getTransactionReceipt(txHash);
            //   } while(receipt == null);

            deadline = 0;
            await web3.eth.getBlock('latest', (error, block) => {
              // transaction expires in 300 seconds (5 minutes)
              deadline = block.timestamp + 1000 * 60 * 20;
            });

            const result = router.methods.swapExactTokensForTokens(
              amountInString,
              amountOutMin,
              [SELL_TOKEN_ADDRESS, BUY_TOKEN_ADDRESS],
              RECEIVE_WALLET_ADDRESS,
              deadline,
              // {
              //  gasLimit: tx.gas,
              //          // gasPrice: (gasPrice * 10**9).toString()
              //          gasPrice: tx.gasPrice
              //  }
            );

            // console.log("buy new token transaction hash: " + result.hash);

            const trans = {
              // this could be provider.addresses[0] if it exists
              from: SELL_WALLET_ADDRESS,
              // target address, this could be a smart contract address
              to: PANCAKE_ROUTER_ADDRESS,
              // optional if you want to specify the gas limit
              gas: tx.gas,
              gasPrice: tx.gasPrice,
              // optional if you are invoking say a payable function
              value: amountInString,
              // this encodes the ABI of the method and the arguements
              data: result.encodeABI(),
            };

            const signedTx = await signer.signTransaction(trans);

            await web3.eth
              .sendSignedTransaction(signedTx.rawTransaction)
              .on('transactionHash', (hash) => {
                console.log('transactionHash : ', hash);
              })
              .on('confirmation', (confirmationNumber, receipt) => {
                console.log('transaction is done');
                console.log('Transaction receipt: ', receipt);
              })
              .on('receipt', (receipt) => {
                console.log('Transaction receipt: ', receipt);
              })
              .on('error', (error, receipt) => {
                // If the transaction was rejected by the network with a receipt,
                // the second parameter will be the receipt.
                console.log('Attack failed: ', error);
                console.log('Transaction receipt: ', receipt);
              });

            // var receiptbuytoken = null;
            // do {
            //      receiptbuytoken = await web3.eth.getTransactionReceipt(result.hash);
            // } while (receiptbuytoken == null);

            // console.log("success buy token");
          } else if (
            tx.input.includes(addLiquidityETHMethodId)
            && tx.input.includes(BUY_TOKEN_ID)
          ) {
            // addLiquidityETH

            // let addedToken = '0x' + (params[0].substring(24, 64));
            flag = 1;
            console.log('Detect addLiquidity!!!');
            console.log('type 2');
            // let abi = ["function approve(address _spender, uint256 _value)
            //       public returns (bool success)"];
            // let contract = new ethers.Contract(token_to_buy, abi, signer);
            // contract.approve(RECEIVE_ADDRESS, (amountOut * (10**18)).toString());
            // console.log("Waiting for addLiquidity done...");

            // var receipt = null;
            //   do {
            //     receipt = await web3.eth.getTransactionReceipt(txHash);
            //   } while(receipt == null);

            deadline = 0;
            await web3.eth.getBlock('latest', (error, block) => {
              // transaction expires in 1200 seconds (20 minutes)
              deadline = block.timestamp + 60 * 20;
            });

            const result = router.methods.swapExactETHForTokens(
              amountOutMin,
              [SELL_TOKEN_ADDRESS, BUY_TOKEN_ADDRESS],
              RECEIVE_WALLET_ADDRESS,
              deadline,
              // {
              //  gasLimit: tx.gas,
              //          // gasPrice: (gasPrice * 10**9).toString()
              //          gasPrice: tx.gasPrice
              // }
            );

            const trans = {
              // this could be provider.addresses[0] if it exists
              from: SELL_WALLET_ADDRESS,
              // target address, this could be a smart contract address
              to: PANCAKE_ROUTER_ADDRESS,
              // optional if you want to specify the gas limit
              gas: tx.gas,
              gasPrice: tx.gasPrice,
              // optional if you are invoking say a payable function
              value: amountInString,
              // this encodes the ABI of the method and the arguements
              data: result.encodeABI(),
            };

            const signedTx = await signer.signTransaction(trans);

            await web3.eth
              .sendSignedTransaction(signedTx.rawTransaction)
              .on('transactionHash', (hash) => {
                console.log('transactionHash : ', hash);
              })
              .on('confirmation', (confirmationNumber, receipt) => {
                console.log('transaction is done');
                console.log('Transaction receipt: ', receipt);
              })
              .on('receipt', (receipt) => {
                console.log('Transaction receipt: ', receipt);
              })
              .on('error', (error, receipt) => {
                // If the transaction was rejected by the network with a receipt,
                // the second parameter will be the receipt.
                console.log('Attack failed: ', error);
                console.log('Transaction receipt: ', receipt);
              });
            // .then(receipt => console.log("Transaction receipt: ", receipt))
            // .catch(err => console.error(err));

            // var receiptbuytoken = null;
            // do {
            //      receiptbuytoken = await web3.eth.getTransactionReceipt(result.hash);
            // } while (receiptbuytoken == null);

            // console.log("success buy token");
          }

          // console.log("aaaa");
          // console.log("params: "+params);

          // console.log('TX hash: ',txHash ); // transaction hash
          // console.log('TX confirmation: ',tx.transactionIndex );
          // "null" when transaction is pending
          // console.log('TX nonce: ',tx.nonce );
          // number of transactions made by the sender prior to this one
          // console.log('TX block hash: ',tx.blockHash );
          // hash of the block where this transaction was in. "null" when transaction is pending
          // console.log('TX block number: ',tx.blockNumber );
          // number of the block where this transaction was in. "null" when transaction is pending
          // console.log('TX sender address: ',tx.from ); // address of the sender
          // console.log('TX value: ',tx.value ); // address of the sender
          // console.log('TX amount(in Ether): ',web3.utils.fromWei(tx.value, 'ether'));
          // value transferred in ether
          // console.log('TX date: ',new Date()); // transaction date
          // console.log('TX gas price: ',tx.gasPrice ); // gas price provided by the sender in wei
          // console.log('TX gas: ',tx.gas ); // gas provided by the sender.
          // console.log('TX input: ',tx.input ); // the data sent along with the transaction.
          // console.log('====================================='); // a visual separator
        }
      }
    } catch (err) {
      console.error(err);
    }
  });
});

// web3.eth.filter("pending").watch(
//     function(error,result){
//         if (!error) {
//             console.log(result);
//         }
//     }
// )
